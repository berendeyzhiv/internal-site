from django.contrib import admin

from .models import Project


@admin.register(Project)
class UserAdmin(admin.ModelAdmin):

    # ЖЕСТЬ: кириллически названная функция для русского заголовка 
    # хз пока-что, как сделать иначе
    def руководители(self):
        # в списке проектов показать колонку с руководителями для каждого проекта
        return ', '.join([h.__str__() for h in self.heads.all()])

    list_display = (
        'title', 'description', руководители
    )
