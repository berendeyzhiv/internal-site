from django.db import models


class Project(models.Model):
    """Model representing a project"""
    title = models.CharField('Название', max_length=100, db_index=True)
    description = models.TextField('Описание', blank=True)
    
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'