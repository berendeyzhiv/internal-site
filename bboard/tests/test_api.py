from itertools import chain

from django.test import TestCase
from django.urls import reverse

from rest_framework import status

from users.models import User
from bboard.models import Project
from bboard.serializers import ProjectSerializer
from users.serializers import (
    BossSerializer, SubordinatesSerializer
)


class TestIndex(TestCase):
    def setUp(self):
        # create 2 projects, 2 bosses and 1 employees
        self.project1 = Project.objects.create(title='project1') #
        self.project2 = Project.objects.create(title='project2')
        self.boss1 = User.objects.create(
            username='boss1', email='boss1@user.com', password='123', is_boss=True  #
        )
        self.boss2 = User.objects.create(
            username='boss2', email='boss2@user.com', password='321', is_boss=True
        )
        self.employee1 = User.objects.create(
            username='emp1', email='emp2@user.com', password='345', is_employee=True  #
        )
        self.project1.heads.add(self.boss1)
        self.boss2.projects.add(self.project1, self.project2)


    def test_index(self):
        url = reverse('index')
        response = self.client.get(url)
        heads = User.objects.filter(is_boss=True)
        projects = Project.objects.all()
        heads_serializer = BossSerializer(heads, many=True).data
        projects_serializer = ProjectSerializer(projects, many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(heads_serializer, response.data.get('heads'))
        self.assertEqual(projects_serializer, response.data.get('projects'))
        boss1_has_project1 = heads_serializer[0].get('projects')
        self.assertEqual(1, len(boss1_has_project1))
        boss2_has_project1_and_project2 = heads_serializer[1].get('projects')
        self.assertEqual(2, len(boss2_has_project1_and_project2))
        self.assertEqual(True, self.employee1.username not in response.data)


class TestDetail(TestCase):
    def setUp(self):
        # create 2 projects, 3 bosses and 3 employees
        self.project1 = Project.objects.create(title='project1') #
        self.project2 = Project.objects.create(title='project2')
        self.project3 = Project.objects.create(title='project3')
        self.boss1 = User.objects.create(
            username='boss1', email='boss1@user.com', password='123', is_boss=True  #
        )
        self.boss2 = User.objects.create(
            username='boss2', email='boss2@user.com', password='321', is_boss=True
        )
        self.employee1 = User.objects.create(
            username='emp1', email='emp2@user.com', password='345', is_employee=True  #
        )
        self.employee2 = User.objects.create(
            username='emp2', email='emp2@user.com', password='346', is_employee=True
        )
        self.employee3 = User.objects.create(
            username='emp3', email='emp3@user.com', password='347', is_employee=True
        )
        self.boss1.projects.add(self.project1)
        self.boss2.projects.add(self.project2, self.project3)
        self.boss1.subordinates.add(self.employee1)
        self.boss2.subordinates.add(self.employee2, self.employee3)

    def test_boss_detail(self):
        # 200 0k
        url = reverse('boss-detail', args=(self.boss1.id,))
        response = self.client.get(url)
        subordinates = self.boss1.subordinates.all()
        projects = self.boss1.projects.all()
        subordinates_serializer = SubordinatesSerializer(subordinates, many=True).data
        projects_serializer = ProjectSerializer(projects, many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(subordinates_serializer, response.data.get('subordinates'))
        self.assertEqual(projects_serializer, response.data.get('projects'))
        # 404 not found 
        url = reverse('boss-detail', args=(100,))
        response = self.client.get(url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        url = reverse('boss-detail', args=(self.employee1.id,))
        response = self.client.get(url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_project_detail(self):
        # 200 0k
        url = reverse('project-detail', args=(self.project1.id,))
        response = self.client.get(url)
        heads = self.project1.heads.all()
        all_subordinates = [head.subordinates.all() for head in heads]
        none_qs = Project.objects.none()
        employees = list(chain(none_qs, all_subordinates))
        heads_serializer = BossSerializer(heads, many=True).data
        employees_serializer = SubordinatesSerializer(*employees, many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(heads_serializer, response.data.get('heads'))
        self.assertEqual(employees_serializer, response.data.get('employees'))
        # 404 not found 
        url = reverse('boss-detail', args=(100,)) 
        response = self.client.get(url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        url = reverse('boss-detail', args=(self.employee1.id,))
        response = self.client.get(url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)