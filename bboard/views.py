from itertools import chain

from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from users.models import User
from bboard.models import Project
from users.serializers import BossSerializer, SubordinatesSerializer
from bboard.serializers import ProjectSerializer
 

class IndexList(APIView):
    def get(self, request):
        heads = User.objects.filter(is_boss=True)
        projects = Project.objects.all()
        heads_serializer = BossSerializer(heads, many=True)
        projects_serializer = ProjectSerializer(projects, many=True)
        bosses_and_projects = {
            'heads': heads_serializer.data,
            'projects': projects_serializer.data
        }
        return Response(bosses_and_projects)


class BossDetail(APIView):
    def get(self, request, pk):
        boss = get_object_or_404(User, id=pk, is_boss=True)
        subordinates = boss.subordinates.all()
        projects = boss.projects.all()
        subordinates_serializer = SubordinatesSerializer(subordinates, many=True)
        projects_serializer = ProjectSerializer(projects, many=True)
        subordinates_and_projects = {
            'subordinates': subordinates_serializer.data,
            'projects': projects_serializer.data
        }
        return Response(subordinates_and_projects)


class ProjectDetail(APIView):
    def get(self, request, pk):
        project = get_object_or_404(Project, id=pk)

        heads = project.heads.all()
        all_subordinates = [head.subordinates.all() for head in heads]
        none_qs = Project.objects.none()
        employees = list(chain(none_qs, all_subordinates))

        heads_serializer = BossSerializer(heads, many=True)
        employees_serializer = SubordinatesSerializer(*employees, many=True)
        subordinates_and_projects = {
            'heads': heads_serializer.data,
            'employees': employees_serializer.data
        }
        return Response(subordinates_and_projects)
    