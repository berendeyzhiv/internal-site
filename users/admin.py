from django.contrib import admin

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):

    def leader(self):
        return self.leader if self.is_employee and self.leader else '---'
    
    list_display = (
        'username', 'email', 'is_boss', 'is_employee', leader, 'is_staff', 'is_superuser'
    )
    list_filter = ('is_boss', 'is_employee', 'is_staff')
