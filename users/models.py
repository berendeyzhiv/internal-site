from django.db import models
from django.contrib.auth.models import AbstractUser

from bboard.models import Project


class User(AbstractUser):
    """auth/login-related fields"""
    is_boss = models.BooleanField(default=False)
    is_employee = models.BooleanField(default=False)
    leader = models.ForeignKey(
        'self', blank=True, null=True, on_delete=models.SET_NULL,
        related_name='subordinates', verbose_name='Сотрудники',
    )
    projects = models.ManyToManyField(
        Project, blank=True, related_name='heads', verbose_name='Руководители проекта'
    )

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'