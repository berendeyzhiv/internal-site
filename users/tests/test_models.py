from django.test import TestCase

from ..models import User
from bboard.models import Project


class ModelsTestCase(TestCase):

    def setUp(self):
        # create 2 projects, 2 bosses and 2 employees
        self.project1 = Project.objects.create(title='project1') #
        self.project2 = Project.objects.create(title='project2')
        self.boss1 = User.objects.create(
            username='boss1', email='boss1@user.com', password='123', is_boss=True  #
        )
        self.boss2 = User.objects.create(
            username='boss2', email='boss2@user.com', password='321', is_boss=True
        )
        self.employee1 = User.objects.create(
            username='emp1', email='emp2@user.com', password='345', is_employee=True  #
        )
        self.employee2 = User.objects.create(
            username='emp2', email='emp2@user.com', password='543', is_employee=True
        )

    def test_m2m_relationship_between_users_and_projects(self):
        self.project1.heads.add(self.boss1)
        self.boss2.projects.add(self.project2)
        # project1 has only boss2  
        self.assertEqual(*self.project1.heads.all(), self.boss1)
        self.assertNotEqual(*self.project1.heads.all(), self.boss2)
        # project2 has only boss1  
        self.assertEqual(*self.boss2.projects.all(), self.project2)
        self.assertNotEqual(*self.project2.heads.all(), self.boss1)
        # get the project through employee.leader
        self.employee1.leader = self.boss2
        self.employee1.save()
        project2, = self.employee1.leader.projects.all()
        self.assertEqual(project2.title, 'project2')

    def test_fk_relationship_between_users(self):
        self.employee1.leader=self.boss1
        self.employee1.save()
        self.employee2.leader=self.boss2
        self.employee2.save()
        # employee1.fk -> boss1 
        self.assertEqual(self.employee1.leader, self.boss1)
        self.assertNotEqual(self.employee1.leader, self.boss2)
        # boss2.reverse-fk -> employee2
        self.assertEqual(*self.boss2.subordinates.all(), self.employee2)
        self.assertNotEqual(*self.boss2.subordinates.all(), self.employee1)
        # employee2.fk -> boss2
        self.assertEqual(self.employee2.leader, self.boss2)
        # project2.m2m -> boss1.fk -> employee1
        self.boss1.projects.add(self.project2)
        boss1 = self.project2.heads.get(id=self.boss1.id)
        bos1_has_employee1 = boss1.subordinates.all()
        self.assertEqual(*bos1_has_employee1, self.employee1)
        